const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow
const { Menu, Tray, dialog} = require('electron')
// const keytar = require('keytar')
const path = require('path')
const url = require('url')
// const fs = require('fs')

const appFolder = path.dirname(process.execPath)
const exeName = path.basename(process.execPath)
const updateExe = path.resolve(appFolder, '..', 'Update.exe')

const AutoLaunch = require('auto-launch')
const pmsLauncher = new AutoLaunch({
    name: 'Pinease-Mksofttech - PMS'
});

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow = null
let isQuitting = false
let tray = null
let settingWindow = null
let settingHidden = true;
const trayIconDefault = path.join(__dirname, 'static/icon.png')
const trayIconWindows = path.join(__dirname, 'static/icon.png')


app.requestSingleInstanceLock()
app.setAppUserModelId('com.pms.v1')
app.on('second-instance', () => {
   //whaat if there is another instance
})


function createWindow() {

    /*if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }*/
    mainWindow = new BrowserWindow({
        width: 1100,
        height: 700,
        minHeight: 600,
        minWidth: 512,
        title: "PMS - Pinease-Mksofttech Project Management System",
        icon: __dirname + '/static/pinicon.png',
        webPreferences: {
            nodeIntegration: true,
            webviewTag: true
        }
    });
    mainWindow.setMenu(null);

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'views/index.html'),
        protocol: 'file:',
        slashes: true
    }))

    // Open the DevTools.
    mainWindow.webContents.openDevTools();

    mainWindow.on('close', e => {
        if (!isQuitting) {
            e.preventDefault()

            if (process.platform === 'darwin') {
                app.hide()
            } else {
                mainWindow.hide()
            }
        }
    })

    mainWindow.on('page-title-updated', e => {
        e.preventDefault()
    })

    return mainWindow

}

function createSettingWindow(){

    settingWindow = new BrowserWindow({
        width: 600,
        height: 512,
        minHeight: 600,
        minWidth: 512,
        icon: __dirname + '/static/pinicon.png',
        webPreferences: {
            nodeIntegration: true
        },
        title: "PMS - Pinease-Mksofttech Project Management System",
        // parent: mainWindow, 
        // modal: true, 
        show: false
    });
    settingWindow.setMenu(null);
    // settingWindow.setResizable(false);
    // settingWindow.center()
    settingWindow.on('will-resize', (e) => {
        //prevent resizing even if resizable property is true.
        e.preventDefault();
    });
    settingWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'views/settings.html'),
        protocol: 'file:',
        slashes: true
    }))

    settingWindow.webContents.openDevTools();
    settingWindow.on('close', e => {
        if (!isQuitting) {
            e.preventDefault()

            if (process.platform === 'darwin') {
                app.hide()
            } else {
                settingWindow.hide()
            }
        }
    })
}

// ================================= APP ON READY ================================
app.on('ready', function () {
    if (process.platform === 'win32') {
        app.setAppUserModelId(process.execPath);
    }
    createWindow();
    createSettingWindow();

    if (process.platform === 'darwin' || tray) {
        return
    }
    let icon = trayIconDefault

    if (process.platform === 'win32') {
        icon = trayIconWindows
    }

    tray = new Tray(icon)
    // checkout : https://pms.pinease.com/attendance/log_time/0
    // checkin : https://pms.pinease.com/attendance/log_time
    const contextMenu = Menu.buildFromTemplate([
        { label: "Checkin", type: "normal" },
        { label: "Checkout", type: "normal" },
        { label: "Settings", type: "normal", 
            click: function () {
                // showSettings();
                if (!settingWindow.isVisible() ){
                    settingWindow.show()
                }
                else {
                    settingWindow.focus()
                }
            } 
        },
        { type: 'separator' },
        { label: "Exit", type: "normal", click: function () {
                confirm_exit();
            }
        }
    ]);

    tray.setToolTip(`Pinease-Mksofttech - PMS`)
    tray.setContextMenu(contextMenu)

    // tray.setContextMenu(Menu.buildFromTemplate(contextMenu))
    tray.on('click', function handleClicked() {
        toggleWindow(mainWindow)
    })

    //============================= on ready functions================================

    const confirm_exit = () => {

        let options = {
            type: 'warning',
            buttons: ["Yes", "No"],
            title: 'Pinease-Mksofttech - PMS',
            message: "Do you really want to quit?",
            icon: trayIconDefault
        }
        let response = dialog.showMessageBox(options, i => {
            if (i == 0) {
                // console.log('under i')
                mainWindow.destroy();
                settingWindow.destroy();
                app.quit();
                app.exit(0)
            }
        })
    }
})


// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
    if(settingWindow === null){
        createSettingWindow()
    }
})

function enable_on_startup(){
    pmsLauncher.isEnabled()
        .then(function (isEnabled) {
            if (isEnabled) {
                return;
            }
            pmsLauncher.enable();
        })
        .catch(function (err) {
            return "Exit with error" + err
        });
}

function disable_on_startup(){
    pmsLauncher.isEnabled()
        .then(function (isEnabled) {
            if (isEnabled) {
                pmsLauncher.disable();
            }
            // pmsLauncher.enable();
        })
        .catch(function (err) {
            return "Exit with error" + err
        });
}

function toggleWindow(windowObject){
    if (process.platform === 'win32') {
        console.log('toggle win platform win32')
        if (windowObject.isMinimized()) {
            windowObject.restore()
        } else if (windowObject.isVisible()) {
            windowObject.hide()
        } else {
            windowObject.show()
        }
    } else {
        windowObject.isVisible() ? windowObject.hide() : windowObject.show()
    }
}