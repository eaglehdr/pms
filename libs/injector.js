// const keytar = require('keytar')

isLoginPage = false
isDashboard = false
isCheckedin = false
isCheckedOut = false

const isOnline = require('is-online')
const keytar = require('keytar')
const pms_settings = require('electron-settings');
// const Store = require('electron-store');
// const store = new Store();
let username = "";
let password = "";
window.onload = () => {
    window.$ = window.jQuery = require('jquery');
    const appname = require('../package.json').name
    jQuery('button.btn.btn-lg.btn-primary.btn-block.mt15[type="submit"]').click(function () {
        // console.log('my lick ran')
        username = jQuery('form#signin-form input#email').val();
        password = jQuery('form#signin-form input#password').val();
        console.log('username:' + username)
        console.log('passw:' + password)

        var thisdate = new Date();
        fulldate = thisdate.getDate() + '' + thisdate.getMonth() + '' + thisdate.getFullYear();

        pdata = {
                uname : username, 
                pass: password 
        };
        localStorage.setItem('temp_cred', JSON.stringify(pdata));
        // store.set({unam : username, pass: password})
        //savepas = keytar.setPassword('pms_service', username, password)


        // console.log('keytar:' + savepas)
        // if (savepas) {
        //     getpass = keytar.findCredentials('pms_service')

        //     getpass.then((result) => {
                
        //         console.log("result: " + result); // result will be 'secret'
        //     });
        //     // console.log(getpass)
        //     console.debug()
        // }
    })

    jQuery(document).ready(() => {  

        //auto loggin
        let auto_login = pms_settings.get('preferences.auto_login');

        // if(auto_login == true){

        // }

        // injecting BOOTSTRAP snackbar.
        if (jQuery('#snackbar').length < 1){
            jQuery('body').append('<div id="snackbar"></div>');
        }
        let sbstyle = `<style>#snackbar {
                        display: none;
                        min-width: 250px; 
                        margin-left: -250px; 
                        background-color: #333; 
                        color: #fff; 
                        text-align: center; 
                        border-radius: 2px; 
                        padding: 16px; 
                        position: fixed; 
                        z-index: 1; 
                        left: 50%; 
                        bottom: 30px; 
                        }

                        #snackbar.show {
                        visibility: visible; 
                        }
                        button#save_pass_keytar {
                            color: #fff;
                            background: #337ab7;
                            border: 1px solid #337ab7;
                            border-radius: 5px;
                            padding: 5px 10px;
                            width: 60px;
                        }

                        button.dismiss_sb {
                            color: #337ab7;
                            background: #fff;
                            border: 1px solid #ccc;
                            border-radius: 5px;
                            padding: 5px 10px;
                            width: 50px;
                        } 
                        select#login_select {
                            color: #000;
                            padding: 5px 10px;
                            border-radius: 5px;
                        }
                        button.acc_login {
                            color: #fff;
                            background: #337ab7;
                            border: 1px solid #337ab7;
                            border-radius: 5px;
                            padding: 5px 15px;
                            height: 32px;
                        }
                        </style>`;
        jQuery('head').append(sbstyle);

        // if (jQuery('div#js-clock-in-out').length !== -1){
            let tempcred = localStorage.getItem('temp_cred');

            if(tempcred){
                let creds = JSON.parse(tempcred);
                let uname = creds.uname

                let users_pass = keytar.getPassword('pms_service', uname)
                
                users_pass.then((result) => {
                    if (!result) {
                        var msg = `<div clas="snack_text"> Do you want to save password for <strong>${creds.uname}</strong>? <button type="button" id="save_pass_keytar">Yes</button> <button type="button" class="dismiss_sb">No</button></div>`
                        jQuery('#snackbar').html(msg)
                        jQuery('#snackbar').show(300)
                    }
                });
        
                // show_snackbar(msg)
            }
        // }

        // buttons on snackbar.. yes for save , no to skip
        jQuery('button#save_pass_keytar').click(function(){
            savepas = keytar.setPassword('pms_service', username, password)
            
            if(savepas){
                getpass = keytar.getPassword('pms_service', username)
                
                getpass.then((result) => {
                    if(result){
                        var msg = `<div clas="snack_text"> Your password has been saved against "<strong>${creds.uname}</strong>"</div>`;
                        jQuery('#snackbar').html(msg)
                       
                        setTimeout(() => {
                            jQuery('#snackbar').hide(200)
                        }, 5000);
                    }
                    
                });
            // console.log(getpass)
            }
        })
        jQuery('button#dismiss_sb').click(function(){
            jQuery('#snackbar').hide();
            localStorage.removeItem('temp_cred');
        })


        if (jQuery('body .signin-box').length > 0){
            isLoginPage = true;
            isDashboard = false;
        }
        else {
            isDashboard = true;
            isLoginPage = false;
        }

        if(isDashboard){
            console.log('is dashboard')
            console.log('username:' + username)
            console.log('passw:' + password)
        }
    
        if(isLoginPage){
            console.log('is loginpage')
            localStorage.clear();
  
            getpass = keytar.findCredentials('pms_service')
            var ktp = ""
            var msg = ""
            if (getpass) {
                getpass.then((result) => {
                    console.log(result); // result will be 'secret'
                    
                    result.map((element, key) => {
                        ktp += `<option data-key="${key}" data-pass="${element.password}">${element.account}</option>`;
                    });

                    msg = `<div clas="snack_text">Login using account: <select id="login_select">`+ktp+`</select> <button class="acc_login">Login</button></div>`;
                    jQuery('#snackbar').html(msg)
                    jQuery('#snackbar').show(200)
                });
                // ktp += "</select>"

                
            }
       
            //get username and password from storage      
            
            jQuery('body').on('click','button.acc_login', function(){
                let uname = jQuery('select#login_select option:selected').text()
                let passw = jQuery('select#login_select option:selected').attr('data-pass')

                jQuery('form#signin-form input#email').val(uname)
                jQuery('form#signin-form input#password').val(passw)

                if (jQuery('form#signin-form input#email').val() != "" && jQuery('form#signin-form input#password').val() != ""){
                    jQuery('form#signin-form button[type="submit"]').click();
                }

                // alert('Username: '+uname+'\nPassword: '+passw)
            })
        }
       
        //getting username and password on signin click
    })
}

show_snackbar = function (msg) {
    // var snackbar = document.createElement("div");
    var snackbar = document.getElementById("snackbar");
    // snackbar.id = "snackbar";
    snackbar.innerHTML = msg
    snackbar.className = "show";
    setTimeout(function () { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
}