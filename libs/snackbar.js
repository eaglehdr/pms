exports.show_snackbar = function (msg) {
    // var snackbar = document.createElement("div");
    var snackbar = document.getElementById("snackbar");
    // snackbar.id = "snackbar";
    snackbar.innerHTML = msg
    snackbar.className = "show";
    setTimeout(function () { snackbar.className = snackbar.className.replace("show", ""); }, 3000);
}